#ifndef FLUTTER_PLUGIN_ANDROID_PLUGIN_H_
#define FLUTTER_PLUGIN_ANDROID_PLUGIN_H_

#include <flutter/method_channel.h>
#include <flutter/plugin_registrar_windows.h>

#include <memory>

namespace android_plugin {

class AndroidPlugin : public flutter::Plugin {
 public:
  static void RegisterWithRegistrar(flutter::PluginRegistrarWindows *registrar);

  AndroidPlugin();

  virtual ~AndroidPlugin();

  // Disallow copy and assign.
  AndroidPlugin(const AndroidPlugin&) = delete;
  AndroidPlugin& operator=(const AndroidPlugin&) = delete;

  // Called when a method is called on this plugin's channel from Dart.
  void HandleMethodCall(
      const flutter::MethodCall<flutter::EncodableValue> &method_call,
      std::unique_ptr<flutter::MethodResult<flutter::EncodableValue>> result);
};

}  // namespace android_plugin

#endif  // FLUTTER_PLUGIN_ANDROID_PLUGIN_H_
