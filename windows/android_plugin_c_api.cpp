#include "include/android_plugin/android_plugin_c_api.h"

#include <flutter/plugin_registrar_windows.h>

#include "android_plugin.h"

void AndroidPluginCApiRegisterWithRegistrar(
    FlutterDesktopPluginRegistrarRef registrar) {
  android_plugin::AndroidPlugin::RegisterWithRegistrar(
      flutter::PluginRegistrarManager::GetInstance()
          ->GetRegistrar<flutter::PluginRegistrarWindows>(registrar));
}
