//
//  Generated file. Do not edit.
//

// clang-format off

#include "generated_plugin_registrant.h"

#include <android_plugin/android_plugin_c_api.h>

void RegisterPlugins(flutter::PluginRegistry* registry) {
  AndroidPluginCApiRegisterWithRegistrar(
      registry->GetRegistrarForPlugin("AndroidPluginCApi"));
}
