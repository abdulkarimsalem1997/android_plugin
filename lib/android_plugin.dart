import 'dart:async';
import 'package:flutter/services.dart';

class AndroidPlugin {
  static const MethodChannel _channel = MethodChannel('android_plugin');

  static Future<String?> fetchQuestions() async {
    final String? response = await _channel.invokeMethod('fetchQuestions');
    return response;
  }
}
