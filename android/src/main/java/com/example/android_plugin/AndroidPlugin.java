package com.example.android_plugin;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;


public class AndroidPlugin implements FlutterPlugin, MethodChannel.MethodCallHandler, ActivityAware {
  private MethodChannel channel;
  private Context context;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "android_plugin");
    channel.setMethodCallHandler(this);
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull MethodChannel.Result result) {
    if (call.method.equals("fetchQuestions")) {
      fetchQuestions(result);
    } else {
      result.notImplemented();
    }
  }

  private void fetchQuestions(MethodChannel.Result result) {
    OkHttpClient client = new OkHttpClient();
    Request request = new Request.Builder()
            .url("https://api.stackexchange.com/2.3/questions?order=desc&sort=activity&site=stackoverflow")
            .build();

    client.newCall(request).enqueue(new Callback() {
          @Override
          public void onFailure(Request request, IOException e) {
            result.error("UNAVAILABLE", "API call failed", null);
          }

          @Override
          public void onResponse(Response response) throws IOException {
            if (response.isSuccessful()){
              new Handler(Looper.getMainLooper()).post(() ->
                      Toast.makeText(context, "Data retrieved successfully", Toast.LENGTH_LONG).show()
              );
              result.success(response.body().string());

            }else {
              result.error("UNAVAILABLE", "API call failed", null);
            }

          }
      }
    );
  }

  @Override
  public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
    context = binding.getActivity();
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {}

  @Override
  public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {}

  @Override
  public void onDetachedFromActivity() {}
}
